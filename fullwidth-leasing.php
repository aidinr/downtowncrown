<?php
/**
 *
Template Name: leasing
 *
 * The archive template. Used when a category, author, or date is queried.
 * Note that this template will be overridden by category.php, author.php, and date.php for their respective query types. 
 *
 * More detailed information about template’s hierarchy: http://codex.wordpress.org/Template_Hierarchy
 *
 */


?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head><div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=401426873260504";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<meta charset="<?php bloginfo('charset') ?>" />
<title>Events | Downtown Crown</title>



<link rel="stylesheet" href="/wp-content/themes/downtowncrown/style.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/wp-content/themes/downtowncrown/css/footer.css" type="text/css" media="screen" /><!-- Footer Stylesheet -->

<script type="text/javascript" src="/wp-content/themes/downtowncrown/js/jquery.js"></script><!-- jQuery -->
<script type="text/javascript" src="/wp-content/themes/downtowncrown/js/footer_plugins.js"></script><!-- HoverIntent -->
<script type="text/javascript" src="/wp-content/themes/downtowncrown/js/footer.js"></script><!-- Footer Script -->
<script>
$(document).ready(function($){
	$('#footer').stickyFooter({
		dropup_speed_show : 300, // Time (in milliseconds) to show a drop down
		dropup_speed_hide : 200, // Time (in milliseconds) to hide a drop down
		dropup_speed_delay : 200, // Time (in milliseconds) before showing a drop down
		footer_effect : 'hover_slide', // Drop down effect, choose between 'hover_fade', 'hover_slide', etc.
		footer_click_outside :0, // Clicks outside the drop down close it (1 = true, 0 = false)
		showhidefooter : 'show', // Footer can be hidden when the page loads
		hide_speed : 1000, // Time to hide the footer (in milliseconds) if the 'showhidefooter' option is set to 'hide'
		hide_delay : 2000 // Time before hiding the footer (in milliseconds) if the 'showhidefooter' option is set to 'hide'
	});
});
</script>

<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php
remove_action('wp_head', 'wp_generator');
wp_enqueue_script('jquery');
wp_enqueue_script('template_script', get_bloginfo('template_url') . '/script.js', 'jquery');
if (is_singular() && get_option('thread_comments')) {
	wp_enqueue_script('comment-reply');
}
wp_head();
?>
<script src="/directory/js/swfobject_modified.js" type="text/javascript"></script>
</head>
<body <?php body_class(); ?>>

<div id="wrapper">
<div id="crown-main">
<nav class="crown-nav clearfix">

    <div class="crown-nav-inner">
    <?php
	echo theme_get_menu(array(
			'source' => theme_get_option('theme_menu_source'),
			'depth' => theme_get_option('theme_menu_depth'),
			'menu' => 'primary-menu',
			'class' => 'crown-hmenu'
		)
	);
?> 
        </div>
    </nav>

<?php if(theme_has_layout_part("header")) : ?>
<header class="clearfix crown-header<?php echo (theme_get_option('theme_header_clickable') ? ' clickable' : ''); ?>"><?php get_sidebar('header'); ?>




    <div class="crown-shapes">


            </div>
                <div class="crown-header-inner">
<div class="crown-header-inner1">
                        <img src="/wp-content/uploads/2014/03/dtc-shadow-logo.png" width="294" height="229" border="0" usemap="#Map">
<map name="Map"><area shape="rect" coords="12,2,267,198" href="/">
</map></div>
                    </div>

</header>

<?php endif; ?>


<div class="crown-sheet clearfix">

            <div class="crown-layout-wrapper clearfix">
                <div class="crown-content-layout">

                    <div class="crown-content-layout-row">
                        <?php get_sidebar(); ?>
                        <div class="crown-layout-cell crown-content clearfix">

                        <style>

                        </style>
                        <!-- Leasing page left box /sidebar -->
                        <div class="sidebar-layout4b">
                        	
                        	<div class="crown-layout-cell crown-sidebar1 clearfix">
								<div id="text-8" class="crown-block widget widget_text  clearfix">
								        <div class="crown-blockcontent">
											<div class="textwidget"><div class="sidebar-crown-block12">
								<div id="titlesidebar"><h3> OVERVIEW </h3></div>
								<div id="spacer1">
								<b>SPACE AVAILABLE</b><br>
								Approximately 63,000 square feet 
								including up to 21,000 square feet
								of junior anchor space.
								<br><br>
								<b>GROSS LEASABLE SPACE</b><br>
								Approximately 260,000<br>
								square feet.
								<br><br>
								<b>TRAFFIC COUNTS</b><br>
								79,062 vehicles daily on Sam Eig Highway.
								</div>
								</div>
								</div>                  
								        </div>
								</div><div id="text-9" class="crown-block widget widget_text  clearfix">
								        <div class="crown-blockcontent">
											<div class="textwidget"><div class="sidebar-crown-block1">

								<div id="titlesidebar2"><h3>BROCHURE</h3></div>
								<div id="spacer1">
								<a href="http://downtowncrown.dreamhosters.com/wp-content/uploads/2013/10/DowntownCrown%20Availability%2010%2029%2013.pdf" target="new">
								Download PDF version of the
								retail brochure »</a>
								<br>
								</div>
								</div></div>									            
								        </div>
								</div>
                       		</div>

                        </div>	
                        <div class="crown-content-layout4"> 
                     


			
			<?php get_sidebar('top'); ?>

			<?php

			if (have_posts()) {
				/* Start the Loop */
				while (have_posts()) {
					the_post();
					get_template_part('content', 'page');
					/* Display comments */
					if (theme_get_option('theme_allow_comments')) {
						comments_template();
					}
				}
			} else {
				theme_404_content();
			}

			?>
<?php get_sidebar(); ?>
			<?php get_sidebar('bottom'); ?>
</div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>

<footer class="crown-footer clearfix"><?php get_sidebar('footer'); ?>
  <div class="crown-footer-inner"><?php
global $theme_default_options;
echo do_shortcode(theme_get_option('theme_override_default_footer_content') ? theme_get_option('theme_footer_content') : theme_get_array_value($theme_default_options, 'theme_footer_content'));
?>

</div>
</div>
</div>

</footer>


</div>



<div id="wp-footer">
	<?php wp_footer(); ?>
	<!-- <?php printf(__('%d queries. %s seconds.', THEME_NS), get_num_queries(), timer_stop(0, 3)); ?> -->

</div>



</div>
</div>

</body>
</html>
